package com.example.webclient;

import com.example.webclient.dto.GithubRepo;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;



@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class WebClientApplicationTests {

    @Autowired
    private WebTestClient webTestClient;

    private static final Logger LOG = LoggerFactory.getLogger(WebClientApplicationTests.class);

    @Test
    @Order(1)
    public void testGetAllGithubRepositories() {
        LOG.info(String.valueOf(webTestClient.get().uri("/api/repos")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(GithubRepo.class)));
    }

    @Test
    @Order(2)
    public void testGetSingleGithubRepository() {
        LOG.info(String.valueOf(webTestClient.get()
                .uri("/api/repos/{repo}", "test-webclient-repository")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .consumeWith(response ->
                        Assertions.assertThat(response.getResponseBody()).isNotNull())));
    }


}
