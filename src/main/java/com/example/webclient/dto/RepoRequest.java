package com.example.webclient.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RepoRequest {
    @NotBlank
    private String name;

    private String description;

    @JsonProperty("private")
    private Boolean isPrivate;

    public RepoRequest(@NotBlank String name, String description) {
        this.name = name;
        this.description = description;
    }
}
