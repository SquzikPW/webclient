package com.example.webclient.service;

import com.example.webclient.dto.GithubRepo;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface GithubClientSerivce {

    Flux<GithubRepo> listGithubRepositories();

    Mono<GithubRepo> getGithubRepository(String owner, String repo);

}
